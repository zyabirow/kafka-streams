import datetime
from dateutil.parser import parse as parse_date
import faust
import logging


class ExpediaRecord(faust.Record):
    id: float
    date_time: str
    site_name: int
    posa_container: int
    user_location_country: int
    user_location_region: int
    user_location_city: int
    orig_destination_distance: float
    user_id: int
    is_mobile: int
    is_package: int
    channel: int
    srch_ci: str
    srch_co: str
    srch_adults_cnt: int
    srch_children_cnt: int
    srch_rm_cnt: int
    srch_destination_id: int
    srch_destination_type_id: int
    hotel_id: int


class ExpediaExtRecord(ExpediaRecord):
    stay_category: str


logger = logging.getLogger(__name__)
app = faust.App('kafkastreams', broker='kafka://kafka:9092')
source_topic = app.topic('expedia', value_type=ExpediaRecord)
destination_topic = app.topic('expedia_ext', value_type=ExpediaExtRecord)

def add_stay_category(check_in: str, check_out: str) -> str:
    '''
    takes check_in and check_out datetimes for hotel customers
    and returns categorized stay:
    - longer than 2 weeks "Long stay"
    - 11-14 days: "Standart extended stay"
    - 5-10 days: "Standart stay"
    - 1-4 days: "Short stay"
    - <= 0 days - "Erroneous data"
    '''
    try:
        days = (parse_date(check_out) - parse_date(check_in)).days
    except:
        return "Erroneous data"
    if days >= 15:
        return "Long stay"
    elif days >= 11:
        return "Standart extended stay"
    elif days >= 5:
        return "Standart stay"
    elif days >= 1:
        return "Short stay"
    else:
        return "Erroneous data"



@app.agent(source_topic, sink=[destination_topic])
async def handle(messages):
    async for message in messages:
        if message is None:
            logger.info('No messages')
            continue

        #Transform your records here
        data = message.asdict()
        stay_category = add_stay_category(check_in=message.srch_ci, check_out=message.srch_co)

        yield ExpediaExtRecord(**data, stay_category=stay_category)


if __name__ == '__main__':
    app.main()
