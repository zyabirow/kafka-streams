
# Kafka connect in Kubernetes

### Using terraform to prepare Azure Kubernetes Server (AKS) and it's infrastracture   

* Change directory to terraform directory
  ```bash
  pushd ./terraform
  ```

* Slightly change to terraform/main.tf file in `terraform.backend` block:  
You will be asked to add these parameters anyway, so it looks for me to be best practice to add them in main.tf  
These parameters are:  
  > name of the *resource group* for your azure account    

  > *storage account name* - to store terraform backend


* Initialize Terraform
  ```bash
  terraform init
  ```  

* Evaluate plan:
  ```bash
  terraform plan -out tfplan
  ```

* Apply plan:
  ```bash
  terraform apply "tfplan"
  ```

### Create Docker image

* Login to container registry:
it allows to get access to docker registry, to push image there and to get it from there

  ```bash
  az acr login -n <registry name>
  ```

* Build docker image:
  > *ACR server* - name of your Azure Container Registry 

  > *image name* - name of image, you fully decide how to name it

  > *tag* - just tag, it is version of your image, you fully decide how to name it
  ```bash
  docker build -f .\docker\Dockerfile . -t <ACR server>/<image name>:<tag>
  ```

* Push docker image to ACR: 
  ```bash
  docker push <ACR server>/<image name>:<tag>
  ```
 
## Launch Confluent for Kubernetes

### Create a namespace

* Get credentials for AKS:
  ```
  az aks get-credentials --resource-group <AKS resource group> --name <AKS name>
  ```
  
* Attach docker image to AKS:
  ```
  az aks update -g <AKS resource group> -n <AKS name> --attach-acr <ACR name>
  ```
- Create the namespace to use:

  ```cmd
  kubectl create namespace confluent
  ```

- Set this namespace to default for your Kubernetes context:

  ```cmd
  kubectl config set-context --current --namespace confluent
  ```  


### Install Confluent for Kubernetes

- Add the Confluent for Kubernetes Helm repository:

  ```cmd
  helm repo add confluentinc https://packages.confluent.io/helm
  helm repo update
  ```
- Install Confluent for Kubernetes:

  ```cmd
  helm upgrade --install confluent-operator confluentinc/confluent-for-kubernetes
  ```

### Install Confluent Platform

- Install all Confluent Platform components:

  ```cmd
  kubectl apply -f ./confluent-platform.yaml
  ```

- Install a sample producer app and topic:

  ```cmd
  kubectl apply -f ./producer-app-data.yaml
  ```

### View Control Center

- Set up port forwarding to Control Center web UI from local machine:

  ```cmd
  kubectl port-forward controlcenter-0 9021:9021
  ```

- Browse to Control Center: [http://localhost:9021](http://localhost:9021)
  ![screenshots](screenshots/m12-1.png)

* Investigate the cluster
  ![screenshots](screenshots/m12-2.png)

## Create a kafka topic

- For this task we need to create two topics: "expedia" and "expedia_ext". Both topics should have at least 3 partitions. "Expedia" topic is created for source data and "expedia_ext" will extend it with additional column "stay category" calculated from check in and check out timestamps (will be shown later):
  ![screenshots](screenshots/m12-3.png)
  ![screenshots](screenshots/m12-4.png)
  ![screenshots](screenshots/m12-5.png)

## Prepare the azure connector configuration

* azure connector configuration is stored in `connectors\azure-source-cc-expedia.json` file. It can be configured according [tutorial](https://docs.confluent.io/kafka-connect-azure-blob-storage-source/current/overview.html). This file is pretty similar to this from the previous homework, but it doesn`t contain configurations for datetime column value masking:
  ![screenshots](screenshots/m12-6.png)


## Upload the connector file through the API
* The easiest way to upload configuration to confluent is to use UI.
* Click on Connect tab and "connect" cluster there
* Choose `upload connector using file` and browse to `connectors\azure-source-cc-expedia.json` file.  
  ![screenshots](screenshots/m12-7.png)  
* If everything went ok you will see new connector `expedia` up and run:
  ![screenshots](screenshots/m12-8.png)  

* Go to Topics > expedia > Messages to check the messages we get from connector:
  ![screenshots](screenshots/m12-9.png)  
Everything works!

## Configure "expedia_ext" kafka-stream application
`expedia_ext` topic is filled  by kafka-stream application. To create and configure it we need to do some additional steps:
* Enrich `src/main.py` script with logic we need. In our case we are creating new column `stay_category`. This column is string and takes one of 5 different values depending on difference of hotel check_in and check_out dates.
  *example:*  
  ![screenshots](screenshots/m12-10.png)

* Create and push docker container to ACR (similar to `Create Docker image` stage). To do so please naviagate to `src` folder and run the commands:
  ```bash
  docker build -f .\docker\Dockerfile . -t <ACR server>/<image name>:<tag>
  ```

  ```bash
  docker push <ACR server>/<image name>:<tag>
  ```

* Start kafka stream application container:
  ```cmd
  kubectl create -f kstream-app.yaml
  ```

* Check the result on Confluent UI [http://localhost:9021](http://localhost:9021):  
  *example:*    
  ![screenshots](screenshots/m12-11.png)  
  ![screenshots](screenshots/m12-12.png)  
  ![screenshots](screenshots/m12-13.png)  



## Aggregating data from "expedia_ext" topic  
to finish the task we need to aggregate data groupping it by `stay category` column. We need to calculate hotel count for each category: total count and distinct hotel count. To do so we should configure stream and create resulting table from this stream. I'll be using Confluent UI (ksqlDB > editor) to do so though it is possible to use ksqlcli instead.

* Create stream:   
  ![screenshots](screenshots/m12-14.png)  

* Create resulting table:   
  ![screenshots](screenshots/m12-15.png)  

* Result examining (done via ksqlcli):
  ```
  kubectl exec --stdin --tty ksqldb-0 -- /bin/ksql
  ``` 
  *example:*  
  ![screenshots](screenshots/m12-16.png) 
  ![screenshots](screenshots/m12-17.png)  

